/*
** my_strstr.c for  in /Users/yohanpiron/ETNA/C/day4/piron_y/my_strstr
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Fri Mar 28 12:01:58 2014 PIRON Yohan
** Last update Thu Apr  3 11:48:32 2014 PIRON Yohan
*/
#include <stdlib.h>

int	my_strlen(char *str);

char	*my_strstr(char *str, char *to_find)
{
  int	i;
  int	counter;
  char	*address;

  i = 0;
  counter = 0;

  if (to_find == '\0')
    return (str);
  while (str[i] != '\0')
    {
      if (str[i] == to_find[counter])
	{
	  address = &str[i];
	  counter++;
	}
      else if (counter != my_strlen(to_find))
	counter = 0;
      i++;
    }
  address -= counter + 1;

  if (counter == my_strlen(to_find))
    return(address);
  else
    return (NULL);
}
