/*
** my_str_isalpha.c for  in /Users/yohanpiron/ETNA/C/day4/piron_y/my_str_isalpha
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Fri Mar 28 15:28:44 2014 PIRON Yohan
** Last update Fri Mar 28 21:37:33 2014 PIRON Yohan
*/
int	my_str_isalpha(char *str)
{
  int	i;

  if (str == '\0')
    return (1);

  for (i = 0; str[i] != '\0'; i++)
    {
      if ((str[i] < 'A')  || (str[i] > 'z'))
	return (0);
      if ((str[i] < 'a') && (str[i] > 'Z'))
	return (0);
    }
  return (1);
}
