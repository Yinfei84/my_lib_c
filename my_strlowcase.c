/*
** my_strlowcase.c for  in /Users/yohanpiron/ETNA/C/day4/piron_y/my_strupcase
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Fri Mar 28 14:33:31 2014 PIRON Yohan
** Last update Fri Mar 28 15:06:25 2014 PIRON Yohan
*/
char	*my_strlowcase(char *str)
{
  int	i;

  for (i = 0; str[i] != '\0'; i++)
    {
      if (str[i] > '@' && str[i] < '[' && str[i] != ' ')
	str[i] += 32;
    }

  return (str);
}
