/*
** my_str_isupper.c for  in /Users/yohanpiron/ETNA/C/day4/piron_y/my_str_islower
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Fri Mar 28 15:48:10 2014 PIRON Yohan
** Last update Sat Mar 29 09:19:05 2014 PIRON Yohan
*/
int	my_str_isupper(char *str)
{
  int	i;

  if (str == '\0')
    return (1);

  for (i = 0; str[i] != '\0'; i++)
    {
      if (str[i] < 'A' && str[i] > 'Z')
	return (0);
    }

  return (1);
}
