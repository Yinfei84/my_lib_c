/*
** my_swap.c for my_swap.c in
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Tue Mar 25 09:14:01 2014 PIRON Yohan
** Last update Tue Mar 25 19:01:37 2014 PIRON Yohan
*/
int	my_swap(int *a, int *b)
{
  int	c;

  c = *a;
  *a = *b;
  *b = c;

  return (0);
}
