/*
** my_strncmp.c for  in /Users/yohanpiron/ETNA/C/day4/piron_y/my_strncmp
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Fri Mar 28 13:05:34 2014 PIRON Yohan
** Last update Fri Mar 28 22:56:13 2014 PIRON Yohan
*/
int	my_strncmp(char *s1, char *s2, int n)
{
  int	i;

  for (i = 0; s1[i] != '\0' || s2[i] != '\0'; i++)
    {
      if (i < n)
	{
	  if (s1[i] != s2[i])
	    return (s1[i] - s2[i]);
      	}
    }
  return (0);
}
