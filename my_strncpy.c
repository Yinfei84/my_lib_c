/*
** my_strncpy.c for  in /Users/yohanpiron/ETNA/C/day4/piron_y/my_strncpy
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Fri Mar 28 09:51:08 2014 PIRON Yohan
** Last update Fri Mar 28 10:37:25 2014 PIRON Yohan
*/
char	*my_strncpy(char *dest, char *src, int i)
{
  int	loop;

  for (loop = 0; src[loop] != '\0'; loop++)
    {
      if (i > loop)
	dest[loop] = src[loop];
    }

  if (i > loop)
    dest[loop] = '\0';

  return (dest);
}
