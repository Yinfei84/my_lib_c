/*
** my_strlen.c for my_strlen.c in /Users/yohanpiron/ETNA/C/day2/piron_y/my_strlen
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Tue Mar 25 12:18:27 2014 PIRON Yohan
** Last update Tue Mar 25 19:04:14 2014 PIRON Yohan
*/
int	my_strlen(char *str)
{
  int	i;

  i = 0;

  while (str[i] != '\0')
    {
      i++;
    }
  return (i);
}
