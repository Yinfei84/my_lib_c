/*
** my_sort_int_tab.c for  in /Users/yohanpiron/ETNA/C/day2/piron_y/my_sort_int_tab
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Tue Mar 25 20:09:23 2014 PIRON Yohan
** Last update Tue Mar 25 21:14:02 2014 PIRON Yohan
*/
void	my_sort_int_tab(int *tab, int size)
{
  int	i;
  int	c;

  for (i = 1; i < size; i++)
    {
      while (tab[i] < tab[i-1])
	{
	  c = tab[i];
	  tab[i] = tab[i-1];
	  tab[i-1] = c;
	  i = 1;
	}
    }
}
