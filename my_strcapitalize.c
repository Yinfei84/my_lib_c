/*
** my_strcapitalize.c for  in /Users/yohanpiron/ETNA/C/day4/piron_y/my_strcapitalize
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Fri Mar 28 15:11:06 2014 PIRON Yohan
** Last update Fri Mar 28 21:29:34 2014 PIRON Yohan
*/
char	*my_strcapitalize(char *str)
{
  int	i;

  for (i = 0; str[i] != '\0'; i++)
    {
      if (str[i] >= 'A' && str[i] <= 'Z')
	str[i] += 32;
    }

  for (i = 0; str[i] != '\0'; i++)
    {
      if (i == 0 || str[i - 1] == ' ')
	str[i] -= 32;
    }

  return (str);
}
