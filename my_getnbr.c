/*
** my_getnbr.c for my_getnbr.c in /Users/yohanpiron/ETNA/C/day2/piron_y/my_getnbr
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Tue Mar 25 14:31:33 2014 PIRON Yohan
** Last update Tue Mar 25 20:27:16 2014 PIRON Yohan
*/
int	my_getnbr(char *str)
{
  int i;
  int nb;
  int is_negative;

  is_negative = 1;
  nb = 0;

  for (i = 0; str[i] != '\0'; i++)
    {
      if (str[i] == '-')
	is_negative *= -1;
      if (str[i] == '+')
	{};
      if (str[i] > '9')
	return (nb * is_negative);
      if (str[i] != '+' && str[i] != '-')
	nb = (nb * 10) + (str[i] - '0');
    }
  return (nb * is_negative);
}
