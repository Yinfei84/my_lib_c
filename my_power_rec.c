/*
** my_power_rec.c for  in /Users/yohanpiron/ETNA/C/day3/piron_y/my_power_rec
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Thu Mar 27 11:17:28 2014 PIRON Yohan
** Last update Thu Mar 27 21:13:11 2014 PIRON Yohan
*/
int	my_power_rec(int nb, int power)
{
  if (power == 0)
    return (1);
  else if (power < 0)
    return (0);
  else
    return (nb * my_power_rec(nb, power -1));
}
