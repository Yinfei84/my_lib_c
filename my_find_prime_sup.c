/*
** my_find_prime_sup.c for  in /Users/yohanpiron/ETNA/C/day3/piron_y/my_find_prime_sup
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Thu Mar 27 18:33:15 2014 PIRON Yohan
** Last update Sat Mar 29 17:42:58 2014 PIRON Yohan
*/
int	my_is_prime(int nombre);

int	my_find_prime_sup(int nb)
{
  int	result;

  result = my_is_prime(nb);

  return (result);
}
