/*
** my_putchar.c for my_putchar.c in /Users/yohanpiron/ETNA/C/my_lib
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Sat Mar 29 09:21:57 2014 PIRON Yohan
** Last update Sat Mar 29 09:22:58 2014 PIRON Yohan
*/
#include <unistd.h>
void	my_putchar(char c)
{
  write(1, &c, 1);
}
