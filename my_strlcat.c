/*
** my_strlcat.c for my_strlcat.c in /Users/yohanpiron/ETNA/C/my_lib
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Sat Mar 29 12:22:51 2014 PIRON Yohan
** Last update Sat Mar 29 18:13:04 2014 PIRON Yohan
*/
int	my_strlen(char *str);

int	my_strlcat(char *str1, char *str2, int l)
{
  int	length;
  int	i;

  length = my_strlen(str1);

  if (l < length)
    return (l + my_strlen(str2));

  for (i = 0; (((i + length) < l) && (str2[i] != '\0')); i++)
    str1[i + length] = str2[i];

  if ((i + length) < l)
    str1[i + length] = '\0';
  else
    str1[i + length - 1] = '\0';

  return (length + my_strlen(str2));
}
