/*
** my_put_nbr.c for  in /Users/yohanpiron/ETNA/C/day2/piron_y/my_put_nbr
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Tue Mar 25 12:59:42 2014 PIRON Yohan
** Last update Thu Apr  3 11:42:59 2014 PIRON Yohan
*/
void	my_putchar(char c);

void	maximum_print(char *c)
{
  int	i;

  my_putchar('-');
  for (i = 0; c[i] != '\0'; i++)
    {
      my_putchar(c[i]);
    }
}

void	positive_print(int nb)
{
  int	save;
  int	divi;

  save = nb;
  divi = 1;

  while (nb >= 10)
    {
      nb /= 10;
      divi *= 10;
    }
  while (divi >= 1)
    {
      my_putchar(save / divi + '0');
      save %= divi;
      divi /= 10;
    }
}

void	negative_print(int nb)
{
  int	number;
  if (nb == -2147483648)
    {
      char	*mystring;
      mystring = "2147483648";
      maximum_print(mystring);
    }
  else
    {
      number = nb * -1;
      my_putchar('-');
      positive_print(number);
    }
}

int	my_put_nbr(int nb)
{
  if (nb >= 0)
    {
      positive_print(nb);
    }
  else
    {
      negative_print(nb);
    }
  return (0);
}
