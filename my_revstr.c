/*
** my_revstr.c for  in /Users/yohanpiron/ETNA/C/day4/piron_y/my_revstr
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Fri Mar 28 11:03:30 2014 PIRON Yohan
** Last update Sat Mar 29 17:46:50 2014 PIRON Yohan
*/
int	my_strlen(char *str);

char	*my_revstr(char *str)
{
  int	i;
  int	len;
  char	tmp;

  len = my_strlen(str) - 1;

  for (i = 0; i < len; i++)
    {
      tmp = str[i];
      str[i] = str[len];
      str[len] = tmp;
      len--;
    }

  return (str);
}
