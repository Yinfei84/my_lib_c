/*
** my_showstr.c for my_showstr.c in /Users/yohanpiron/ETNA/C/my_lib
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Sat Mar 29 14:40:21 2014 PIRON Yohan
** Last update Sat Mar 29 17:48:00 2014 PIRON Yohan
*/
void	my_putchar(char c);

int	my_strlen(char *str);

int	my_putnbr_base(int nb, char *base)
{
  int	i;

  if (nb == -2147483648)
    nb -= 1;

  if (nb < 0)
    {
      my_putchar('-');
      nb *= -1;
    }
  i = nb % my_strlen(base);
  nb /= my_strlen(base);

  if (nb > 0)
    my_putnbr_base(nb, base);

  my_putchar(base[i]);
  return (nb);
}

int	my_showstr(char *str)
{
  int	i;

  i = 0;

  while (str[i])
    {
      if (str[i] < 32 || str[i] > 126)
	{
	  my_putchar('\\');
          my_putnbr_base((str[i]), "0123456789abcdef");
	}
      else
	my_putchar(str[i]);
      i++;
    }
  my_putchar('\n');
  return (0);
}
