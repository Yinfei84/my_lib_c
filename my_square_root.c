/*
** my_square_root.c for  in /Users/yohanpiron/ETNA/C/day3/piron_y/my_square_root
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Thu Mar 27 11:43:14 2014 PIRON Yohan
** Last update Thu Mar 27 20:12:43 2014 PIRON Yohan
*/
int	my_square_root(int nb)
{
  int	i;

  i = 1;

  while (i * i != nb)
    {
      if (i > nb)
	return (0);
      else
	i++;
    }
  return (i);
}
