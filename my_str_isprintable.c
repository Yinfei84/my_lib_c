/*
** my_str_isprintable.c for  in /Users/yohanpiron/ETNA/C/day4/piron_y/my_str_isprintable
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Fri Mar 28 16:26:14 2014 PIRON Yohan
** Last update Fri Mar 28 21:41:24 2014 PIRON Yohan
*/
int	my_str_isprintable(char *str)
{
  int	i;

  if (str == '\0')
    return (1);

  for (i = 0; str[i] != '\0'; i++)
    {
      if (str[i] < 32 || str[i] > 126)
	return (0);
    }
  return (1);
}
