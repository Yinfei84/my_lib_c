/*
** my_isneg.c for my_isneg.c in
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Mon Mar 24 12:50:30 2014 PIRON Yohan
** Last update Sat Mar 29 17:44:18 2014 PIRON Yohan
*/
void	my_putchar(char c);

int	my_isneg(int n)
{
  int	my_num;

  my_num = n;

  if (my_num >= 0)
    {
      my_putchar('P');
    }
  else
    {
      my_putchar('N');
    }
  return (0);
}
