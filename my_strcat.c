/*
** my_strcat.c for my_strcat.c in /Users/yohanpiron/ETNA/C/my_lib
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Sat Mar 29 11:22:16 2014 PIRON Yohan
** Last update Sat Mar 29 17:48:53 2014 PIRON Yohan
*/
int	my_strlen(char *str);

char	*my_strcat(char *str1, char *str2)
{
  int	i;
  int	length;

  length = my_strlen(str1);

  for (i = 0; str2[i] != '\0'; i++)
    {
      str1[length] = str2[i];
      length++;
    }

  str1[length] = '\0';

  return (str1);
}
