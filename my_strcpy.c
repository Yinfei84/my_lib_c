/*
** my_strcpy.c for  in /Users/yohanpiron/ETNA/C/day4/piron_y/my_strcpy
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Fri Mar 28 09:06:03 2014 PIRON Yohan
** Last update Fri Mar 28 20:03:37 2014 PIRON Yohan
*/
char	*my_strcpy(char *dest, char *src)
{
  int	i;

  for (i = 0; src[i] != '\0'; i++)
    dest[i] = src[i];

  dest[i] = '\0';

  return (dest);
}
