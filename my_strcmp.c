/*
** my_strcmp.c for  in /Users/yohanpiron/ETNA/C/day4/piron_y/my_strcmp
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Fri Mar 28 12:16:58 2014 PIRON Yohan
** Last update Fri Mar 28 14:05:19 2014 PIRON Yohan
*/
int	my_strcmp(char *s1, char *s2)
{
  int	i;

  for (i = 0; s1[i] != '\0' || s2[i] != '\0'; i++)
    {
      if (s1[i] != s2[i])
	return (s1[i] - s2[i]);
    }
  return (0);
}
