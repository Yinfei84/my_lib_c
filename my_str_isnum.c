/*
** my_str_isnum.c for  in /Users/yohanpiron/ETNA/C/day4/piron_y/my_str_isalpha
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Fri Mar 28 15:28:44 2014 PIRON Yohan
** Last update Sat Mar 29 09:16:04 2014 PIRON Yohan
*/
int	my_str_isnum(char *str)
{
  int	i;

  if (str == '\0')
    return (1);

  for (i = 0; str[i] != '\0'; i++)
    {
      if (str[i] < '0' && str[i] > '9')
	return (0);
    }
  return (1);
}
