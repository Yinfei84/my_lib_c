/*
** my_putstr.c for my_putstr.c in
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Tue Mar 25 09:55:08 2014 PIRON Yohan
** Last update Sat Mar 29 17:46:09 2014 PIRON Yohan
*/
void	my_putchar(char c);

int	my_putstr(char *str)
{
  int	i;

  for (i = 0; str[i] != '\0'; i++)
    my_putchar(str[i]);

  return (0);
}
