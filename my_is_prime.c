/*
** my_is_prime.c for  in /Users/yohanpiron/ETNA/C/day3/piron_y/my_is_prime
**
** Made by PIRON Yohan
** Login   <piron_y@etna-alternance.net>
**
** Started on  Thu Mar 27 12:36:33 2014 PIRON Yohan
** Last update Thu Mar 27 21:16:16 2014 PIRON Yohan
*/
int	my_is_prime(int nombre)
{
  int	i;

  i = 2;
  if (nombre == 1 || nombre <= 0)
    return (0);
  while (i < nombre)
    {
      if (nombre % i == 0)
	return (0);
      i++;
    }
  return (1);
}
